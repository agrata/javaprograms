package folder;

public class ThreadDemo extends Thread {
	
	Thread t;
	
	public ThreadDemo()
	{
		t = new Thread();
		System.out.println("Current Thread"+t.getName());
		//Thread.currentThread().start();
	}
	
	public static void main(String[] args)
	
	{
		Thread t1 = new Thread(new ThreadDemo());
		Thread t2 = new Thread(new ThreadDemo());
		
		t1.start();
		t2.start();
		
		if(t1.isAlive())
		{
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//Thread.currentThread().start();
	}

}
