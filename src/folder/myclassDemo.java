package folder;
abstract class AbsDemo
{
   
   void Print1()
   {
     System.out.println("This is print1");
   }


   void Print2()
   {
     System.out.println("This is print2");
   }
}


class Myclass1 extends AbsDemo
{

   void Print1()
   {
     System.out.println("This is print1");
   }


   void Print2()
   {
     System.out.println("This is print2");
   }
}


class Myclass2 extends AbsDemo
{

   void Print1()
   {
     System.out.println("This is print1");
   }


   void Print2()
   {
     System.out.println("This is print2");
   }

 }

 public class myclassDemo 
 {

     public static void main(String[] args)
     {

       AbsDemo[] ab1 = new Myclass1[0];
       AbsDemo[] ab2 = new Myclass2[1];

       for(int x=0;x<2;++x)
       {
          ab1[x].Print1();
          ab2[x].Print1();
       }
     }
  }
       
