package folder;
public class Circle implements Area
{

   public double radius;

   public Circle()
   {
     radius = 20.0;
   }

   public double area(int radius)
   {
     this.radius = radius;
     return pi*radius*radius;
   }

   public double cir(int radius)
   {
     this.radius = radius;
     return 2*pi*radius;

   }
 
   public static void main(String[] args)
   {
     Circle c = new Circle();
     
     System.out.println("Circle area is = "+c.area(12));
      System.out.println("Circle cirumference is = "+c.cir(12));

    }
 }
     