package folder;
import java.io.*;

 class NewFileDemo
 {

    public static void main(String[] args)throws IOException
    {

       FileWriter fw = new FileWriter("HelloDreamOne.txt");

       BufferedWriter bw = new BufferedWriter(fw);


       bw.write("This is a very good dream");

       bw.close();

    }
  }