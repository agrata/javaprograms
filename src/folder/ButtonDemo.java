package folder;
import javax.swing.*;
import java.awt.*;

 class ButtonDemo
 {

    public static void main(String[] args)
    {

       JFrame f = new JFrame("Hello Button");

       JButton b = new JButton("Hello");


       f.add(b);

       f.setSize(100,100);

       f.setLayout(new FlowLayout());

       f.setVisible(true);

       f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
 }