package folder;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextBoxDemo extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel mylab;
	JTextField myf;
	JButton myb;
	JLabel lab;
	
	JPanel p1,p2;
	
	public TextBoxDemo()
	{
		mylab = new JLabel("Your name");
		myf = new JTextField(20);
		myb = new JButton("Click me");
		lab = new JLabel();
		
		p1 = new JPanel();
		p2 = new JPanel();
		
		myb.addActionListener(this);
		
		p1.add(mylab);
		p1.add(myf);
		p1.add(myb);
		
		p2.add(lab);
		
		p1.setLayout(new FlowLayout(FlowLayout.LEFT));
		p2.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		add(p1);
		add(p2);
		
		setLayout(new GridLayout(2,1));
		validate();
		setVisible(true);
		setSize(500,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		
		
		
	}
	
	
	public static void main(String[] args)
	{
		new TextBoxDemo();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		String se = e.getActionCommand();
		
		if(se.equals("Click me"))
		{
			lab.setText("Hello, "+myf.getText());
		}
		
	}

}
