package folder;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JComboDemo extends JFrame implements ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel which;
	JComboBox<String> cb;
	JLabel th;
	JPanel p;
	
	public JComboDemo()
	{
		which = new JLabel("Which is");
		cb = new JComboBox<String>();
		cb.addItem("select");
		cb.addItem("Largest ocean");
		cb.addItem("Peninsula");
		th = new JLabel();
		p = new JPanel();
		
		cb.addItemListener(this);
		
		p.add(which);
		p.add(cb);
		p.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(p);
		add(th,BorderLayout.SOUTH);
		validate();
		setVisible(true);
		setSize(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
	}
	
	public static void main(String[] args)
	{
		new JComboDemo();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		String s = e.getItem().toString();
		
		if(s.equals("Largest ocean"))
		{
			th.setText("pacific ocean");
			
		}
		else if(s.equals("Peninsula"))
		{
			th.setText("India");
			
			
		}else{
			
		}
		
	}

}
