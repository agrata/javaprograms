package folder;
  import java.awt.*;
  import java.awt.event.*;

  public class LabelDemoEvent implements ActionListener 
  {

    Label lab;  

    public LabelDemoEvent()
    {
      Frame f = new Frame("Hello Button");
      
      Button b = new Button("Hello world");

       lab = new Label("");


      b.setBackground(Color.magenta);
      b.addActionListener(this);

     
      f.add(b);
       f.add(lab);

      /*f.addWindowListener(new WindowEvent(){

       public void Onclose()
       {
         System.Exit(0);

       }});

      }*/


     f.setVisible(true);
     f.setLayout(new FlowLayout());
     f.setSize(100,100);

    }

    public void actionPerformed(ActionEvent ae)
    {
       String s = ae.getActionCommand();

       if(s.equals("Hello world"))
       {
         lab.setText("Hello world");
       }
    
    }
    public static void main(String[] args)
    {
       new LabelDemoEvent();
     
     }
  }