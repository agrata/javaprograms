package folder;

public interface Area
{
  public static final double pi = 3.14;

  public double area(int radius);
  public double cir(int radius);

}