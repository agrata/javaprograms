package folder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

 public class ButtonDemoEvent implements ActionListener
 {
     JLabel lab;

     public ButtonDemoEvent() {

     JFrame f = new JFrame("Hello Button");

       JButton b = new JButton("Hello");

     lab = new JLabel("");

      b.addActionListener(this);


       f.add(b);

       f.add(lab);

       f.setSize(100,100);

       f.setLayout(new FlowLayout());

       f.setVisible(true);

       f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

     }

    public void actionPerformed(ActionEvent ae)
    {
       String s = ae.getActionCommand();
       
       if(s.equals("Hello"))
       {

         lab.setText("Hello Agrata");
 
       }

      
    }


    public static void main(String[] args)
    {
       new ButtonDemoEvent();
      
    }
 }