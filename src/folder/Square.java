package folder;
  class Square
  {
    int edge;

    private int Area(int edge) 
    {
      this.edge = edge;
      return edge*edge;
    }

   private int Peri(int edge)
   {
     this.edge = edge;
     return 4*edge;
   }


   public void Calculate(int edge)
   {
     this.edge = edge;
     System.out.println("Area of square having edge"+Area(edge));
      System.out.println("Perimeter of square having edge"+Peri(edge));
   }


   public static void main(String[] args)
   {
     Square sq = new Square();
     sq.Calculate(10);
    }
  }

   
     